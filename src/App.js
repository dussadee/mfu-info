import React from 'react';
import './App.css';

import GoogleMapReact from 'google-map-react';
import { MdLocationOn, MdDirectionsBus } from 'react-icons/md';
import { TiUser } from 'react-icons/ti';
import { IoMdLocate } from 'react-icons/io';

var dataPlace = require('./MockPlace.json')
const apikey = 'AIzaSyC6d4M6mFY6nqcfF2HFgBqp3p7U7ZNIzqc'

export default class App extends React.Component {
  static defaultProps = {
    zoom: 17
  };

  state = {
    title: '',
    image: '',
    currentPos: [],
    loading: true
  }

  async  componentDidMount() {
    this.getPosition()
  }

  componentDidUpdate() {
    console.log(this.state.center)
  }

  //get user current location 
  getPosition = async () => {
    var pos = []
    await navigator.geolocation.getCurrentPosition((position) => {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      }
      this.setState({ currentPos: pos, loading: false })
      console.log('Hello', this.state.currentPos)
    })
  }

  showTitle = (title, image) => {
    this.setState({ title, image })
  }

  hideTitle = () => {
    this.setState({ title: '', image: '' })
  }

  render() {
    //const { visible } = this.state;
    if (this.state.loading === true) {
      return (
        <div />
      )
    }
    return (
      // Important! Always set the container height explicitly
      <body>
        <div className="googlemap">
          <IoMdLocate style={{ height: 50, width: 50 }} onClick={() => window.location.reload()}></IoMdLocate>
          <GoogleMapReact
            bootstrapURLKeys={{ key: apikey }}
            defaultCenter={this.state.currentPos}
            defaultZoom={this.props.zoom}
          >
            {
              this.state.currentPos &&
              <MarkerUser
                lat={this.state.currentPos.lat}
                lng={this.state.currentPos.lng}
                color="#54A1F3"
                click={() => this.showTitle('My position')}
              />
            }
             {
              dataPlace.map((res, i) => {
                if (res.type === 'building') {
                  return <MarkerPlace
                    key={i}
                    lat={res.lat}
                    lng={res.long}
                    color="#FFB4A5"
                    click={() => this.showTitle(res.title)}
                  />
                } 
                else if (res.type === 'busstop') {
                  return <MarkerBusstop
                    key={i}
                    lat={res.lat}
                    lng={res.long}
                    color="#74EA1C"
                    click={() => this.showTitle(res.title)}
                  />
                }
              })
            }
          </GoogleMapReact>
        </div>
        {this.state.title &&
          <div className="detail-popup" >
            <h3>{this.state.title || 'Hello'}</h3>
            <img src={this.state.image} />
            <div onClick={() => this.hideTitle()}>
              Hide
            </div>
          </div>
        }
      </body>
    );
  }
}

const MarkerPlace = ({ click, color }) => {
  return (
    <div onClick={click}>
      <MdLocationOn size={'5em'} color={color}></MdLocationOn>
    </div>
  )
}

const MarkerBusstop = ({ click, color }) => {
  return (
    <div onClick={click}>
      <MdDirectionsBus size={'5em'} color={color}></MdDirectionsBus>
    </div>
  )
}

const MarkerUser = ({ click, color }) => {
  return (
    <div onClick={click}>
      <TiUser size={'5em'} color={color}></TiUser>
    </div>
  )
}